package main

import (
	"gateway/internal/auth"
	"gateway/internal/endpoint"
	"gateway/internal/logger"
	"gateway/internal/repo"
	"github.com/jackc/pgx"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"github.com/valyala/fasthttp"
	"log"
	"os"
	"os/signal"
)

var (
	dbConn             *pgx.Conn
	usersSessionsTable *repo.UsersSessionsTable

	authorizer *auth.Authorizer

	httpHandler *endpoint.HttpHandler
)

func main() {
	logger.Initialize()
	setupConfig()
	setupDatabase()
	setupTables()
	setupAuthorizer()

	var err error
	httpHandler, err = endpoint.NewHttpHandler(authorizer)
	if err != nil {
		logrus.Fatal(err.Error())
	}

	go func() {
		logrus.Info("Server was started")

		s := &fasthttp.Server{
			Handler:           httpHandler.Handle,
			StreamRequestBody: true,
		}
		err := s.ListenAndServe("0.0.0.0:8000")
		if err != nil {
			logrus.Warn(err.Error())
		}
	}()

	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, os.Interrupt)
	<-sigChan
}

func setupConfig() {
	viper.SetConfigName("configuration")
	viper.SetConfigType("yml")
	viper.AddConfigPath(".")
	err := viper.ReadInConfig()
	if err != nil {
		logrus.Fatal(err.Error())
	}

	// database default values
	viper.SetDefault("sql.hostname", "localhost")
	viper.SetDefault("sql.port", "5432")
	viper.SetDefault("sql.user", "postgres")
}

func setupDatabase() {
	host := viper.GetString("sql.hostname")
	port := viper.GetUint32("sql.port")
	user := viper.GetString("sql.user")
	password := viper.GetString("sql.password")
	databaseName := viper.GetString("sql.database")

	var err error
	dbConn, err = pgx.Connect(pgx.ConnConfig{
		Host:         host,
		Port:         uint16(port),
		Database:     databaseName,
		User:         user,
		Password:     password,
		CustomCancel: func(_ *pgx.Conn) error { return nil },
	})
	if err != nil {
		log.Fatalln("Error occurred during opening database connection: ",
			err.Error())
	}
}

func setupTables() {
	var err error
	usersSessionsTable, err = repo.NewUsersSessionsTable(dbConn)
	if err != nil {
		logrus.Fatal(err.Error())
	}
}

func setupAuthorizer() {
	authorizer = auth.NewAuthorizer(usersSessionsTable)
}
