package auth

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"errors"
	"gateway/internal/repo"
	"gateway/internal/util/cast"
	"github.com/spf13/viper"
	"strconv"
)

var (
	ErrInvalidSession = errors.New("invalid session")
)

const (
	UuidLength = 36
)

type Authorizer struct {
	usersSessionsTable *repo.UsersSessionsTable
	secret             []byte
}

func NewAuthorizer(usersSessionsTable *repo.UsersSessionsTable) *Authorizer {
	return &Authorizer{
		usersSessionsTable: usersSessionsTable,
		secret:             cast.StringToByteArray(viper.GetString("auth.secret")),
	}
}

func (a *Authorizer) getSecret(userId int) []byte {
	idStr := strconv.Itoa(userId)
	secret := make([]byte, len(a.secret)+len(idStr))
	secret = append(secret, a.secret...)
	return append(secret, idStr...)
}

func (a *Authorizer) ValidateSession(session []byte, userAgent, ip, location string) (repo.UserSession, error) {
	if len(session) < UuidLength {
		return repo.UserSession{}, ErrInvalidSession
	}

	sessionMac, err := hex.DecodeString(cast.ByteArrayToString(session[UuidLength:]))
	if err != nil {
		return repo.UserSession{}, ErrInvalidSession
	}

	var userSession repo.UserSession
	userSession, err = a.usersSessionsTable.GetById(cast.ByteArrayToString(session[:UuidLength]))
	if err != nil {
		return repo.UserSession{}, err
	}

	mac := hmac.New(sha256.New, a.getSecret(userSession.UserId))
	mac.Write(session[:UuidLength])
	if !hmac.Equal(mac.Sum(nil), sessionMac) {
		return repo.UserSession{}, ErrInvalidSession
	}

	if userSession.Ip != ip || userSession.Location != location || userSession.UserAgent != userAgent {
		userSession.Ip = ip
		userSession.Location = location
		userSession.UserAgent = userAgent
		err = a.usersSessionsTable.Update(userSession)
		if err != nil {
			return repo.UserSession{}, err
		}
	}

	return userSession, err
}
