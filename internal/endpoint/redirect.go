package endpoint

import (
	"bufio"
	"bytes"
	"gateway/internal/util/cast"
	"github.com/fasthttp/websocket"
	"github.com/sirupsen/logrus"
	"github.com/valyala/fasthttp"
	"io"
	"net/http"
	"strings"
)

func redirectRequestWithUserInfo(ctx *fasthttp.RequestCtx, url string) {
	response, err := sendRequest(ctx, url)
	if err != nil {
		return
	}
	writeResponse(response, ctx)
}

func (h *HttpHandler) webSocketClientToServer(client, server *websocket.Conn) {
	for {
		mt, msg, err := client.ReadMessage()
		if err != nil {
			client.Close()
			server.Close()
			return
		}

		err = server.WriteMessage(mt, msg)
		if err != nil {
			client.Close()
			server.Close()
			return
		}
	}
}

func (h *HttpHandler) webSocketServerToClient(client, server *websocket.Conn) {
	for {
		mt, msg, err := server.ReadMessage()
		if err != nil {
			client.Close()
			server.Close()
			return
		}

		err = client.WriteMessage(mt, msg)
		if err != nil {
			client.Close()
			server.Close()
			return
		}
	}
}

func (h *HttpHandler) redirectRequestWebSocket(ctx *fasthttp.RequestCtx, url string) {
	err := h.websocketsUpgrader.Upgrade(ctx, func(client *websocket.Conn) {
		server, _, err := websocket.DefaultDialer.Dial(url, nil)
		if err != nil {
			client.Close()
			return
		}

		go h.webSocketClientToServer(client, server)
		h.webSocketServerToClient(client, server)
	})
	if err != nil {
		logrus.Error(err.Error())
	}
}

func redirectRequest(ctx *fasthttp.RequestCtx, url string) {
	response, err := sendRequest(ctx, url)
	if err != nil {
		ctx.SetStatusCode(fasthttp.StatusInternalServerError)
		return
	}
	writeResponse(response, ctx)
}

func sendRequest(ctx *fasthttp.RequestCtx, url string) (*http.Response, error) {
	builder := strings.Builder{}
	builder.Grow(len(url) + ctx.QueryArgs().Len() + 1)
	builder.WriteString(url)
	builder.WriteByte('?')
	builder.Write(ctx.QueryArgs().QueryString())
	req, err := http.NewRequest(cast.ByteArrayToString(ctx.Method()), builder.String(), bytes.NewBuffer(ctx.PostBody()))
	if err != nil {
		return nil, err
	}

	ctx.Request.Header.VisitAll(func(k, v []byte) {
		req.Header.Add(cast.ByteArrayToString(k), cast.ByteArrayToString(v))
	})

	return http.DefaultClient.Do(req)
}

func writeResponse(response *http.Response, ctx *fasthttp.RequestCtx) {
	ctx.SetStatusCode(response.StatusCode)
	for header, values := range response.Header {
		for _, v := range values {
			ctx.Response.Header.Add(header, v)
		}
	}

	row, err := io.ReadAll(response.Body)
	if err != nil {
		ctx.SetStatusCode(fasthttp.StatusInternalServerError)
		return
	}

	ctx.SetBody(row)
}

func writeStreamResponse(response *http.Response, ctx *fasthttp.RequestCtx) {
	ctx.SetStatusCode(response.StatusCode)
	for header, values := range response.Header {
		for _, v := range values {
			ctx.Response.Header.Add(header, v)
		}
	}

	ctx.SetBodyStreamWriter(func(w *bufio.Writer) {
		_, _ = w.ReadFrom(response.Body)
		_ = response.Body.Close()
	})
}
