package endpoint

import (
	"encoding/json"
	"errors"
	"gateway/internal/auth"
	"gateway/internal/repo"
	"gateway/internal/util/cast"
	"github.com/fasthttp/websocket"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"github.com/valyala/fasthttp"
	"strconv"
	"strings"
)

const (
	sessionCookieName  = "session"
	userLocationHeader = "User-Location"
	userIdHeader       = "User-Id"
	userRoleHeader     = "User-Role"
)

var ErrInvalidRouterConfig = errors.New("invalid router config")

type HttpHandler struct {
	authorizer         *auth.Authorizer
	router             map[string][]routeConfig
	websocketsUpgrader websocket.FastHTTPUpgrader
}

type routeConfig struct {
	to     string
	method string
	auth   bool
	ws     bool
}

func GetMethod(str string) string {
	switch strings.ToUpper(str) {
	case fasthttp.MethodGet:
		return fasthttp.MethodGet
	case fasthttp.MethodHead:
		return fasthttp.MethodHead
	case fasthttp.MethodPost:
		return fasthttp.MethodPost
	case fasthttp.MethodPut:
		return fasthttp.MethodPut
	case fasthttp.MethodPatch:
		return fasthttp.MethodPatch
	case fasthttp.MethodDelete:
		return fasthttp.MethodDelete
	case fasthttp.MethodConnect:
		return fasthttp.MethodConnect
	case fasthttp.MethodOptions:
		return fasthttp.MethodOptions
	case fasthttp.MethodTrace:
		return fasthttp.MethodTrace
	default:
		return ""
	}
}

func NewHttpHandler(authorizer *auth.Authorizer) (*HttpHandler, error) {
	router := map[string][]routeConfig{}

	for base, baseVal := range viper.GetStringMap("route") {
		baseCfg, ok := baseVal.(map[string]interface{})
		if !ok {
			return nil, ErrInvalidRouterConfig
		}

		cfg := routeConfig{}

		for path, pathVal := range baseCfg {
			var pathCfg map[string]interface{}
			pathCfg, ok = pathVal.(map[string]interface{})
			if !ok {
				return nil, ErrInvalidRouterConfig
			}

		methodLoop:
			for method, methodVal := range pathCfg {
				cfg.method = GetMethod(method)
				if cfg.method == "" {
					return nil, ErrInvalidRouterConfig
				}

				var methodCfg map[string]interface{}
				methodCfg, ok = methodVal.(map[string]interface{})
				if !ok {
					return nil, ErrInvalidRouterConfig
				}

				var strVal string
				strVal, ok = methodCfg["to"].(string)
				if !ok || strVal == "" {
					return nil, ErrInvalidRouterConfig
				}

				cfg.auth, ok = methodCfg["auth"].(bool)
				cfg.ws, ok = methodCfg["ws"].(bool)
				protocol := "http://"
				if cfg.ws {
					if cfg.method != fasthttp.MethodGet {
						return nil, ErrInvalidRouterConfig
					}
					protocol = "ws://"
				}

				urlBuilder := strings.Builder{}
				urlBuilder.Grow(len(protocol) + len(base) + len(path))
				urlBuilder.WriteString(protocol)
				urlBuilder.WriteString(base)
				urlBuilder.WriteString(path)
				cfg.to = urlBuilder.String()

				pathMethods := router[path]
				for _, routeCfg := range pathMethods {
					if routeCfg.method == cfg.method {
						continue methodLoop
					}
				}
				router[path] = append(pathMethods, cfg)
			}
		}
	}

	return &HttpHandler{
		router:     router,
		authorizer: authorizer,
		websocketsUpgrader: websocket.FastHTTPUpgrader{
			WriteBufferSize: 8_388_608,
			ReadBufferSize:  8_388_608,
			CheckOrigin: func(ctx *fasthttp.RequestCtx) bool {
				return true
			},
		},
	}, nil
}

func (h *HttpHandler) Handle(ctx *fasthttp.RequestCtx) {
	defer func() {
		err := recover()
		if err != nil {
			logrus.Error(err)
			ctx.SetStatusCode(fasthttp.StatusInternalServerError)
		}
	}()

	addCorsHeaders(ctx)

	if cast.ByteArrayToString(ctx.Method()) == fasthttp.MethodOptions {
		ctx.SetStatusCode(fasthttp.StatusNoContent)
		return
	}

	configs, ok := h.router[cast.ByteArrayToString(ctx.Path())]
	if !ok {
		ctx.SetStatusCode(fasthttp.StatusNotFound)
		return
	}

	var cfg routeConfig
	ok = false
	for _, cfg = range configs {
		if cfg.method == cast.ByteArrayToString(ctx.Method()) {
			ok = true
			break
		}
	}
	if !ok {
		ctx.SetStatusCode(fasthttp.StatusMethodNotAllowed)
		return
	}
	if cfg.auth {
		var userSession repo.UserSession
		userSession, ok = h.authenticate(ctx)
		if !ok {
			ctx.SetStatusCode(fasthttp.StatusForbidden)
			return
		}

		ctx.Request.Header.Set(userIdHeader, strconv.Itoa(userSession.UserId))
		ctx.Request.Header.Set(userRoleHeader, strconv.Itoa(int(userSession.UserRole)))
	}
	if cfg.ws {
		h.redirectRequestWebSocket(ctx, cfg.to)
	} else {
		redirectRequest(ctx, cfg.to)
	}

}

func (h *HttpHandler) authenticate(ctx *fasthttp.RequestCtx) (repo.UserSession, bool) {
	session := ctx.Request.Header.Cookie(sessionCookieName)

	userAgent := ctx.UserAgent()
	ip := ctx.RemoteIP().String()
	location := ctx.Request.Header.Peek(userLocationHeader)
	if len(userAgent) == 0 || len(location) == 0 {
		writeError(ctx, "no required data", fasthttp.StatusBadRequest)
		return repo.UserSession{}, false
	}
	if len(session) == 0 {
		writeError(ctx, "unauthorized", fasthttp.StatusForbidden)
		return repo.UserSession{}, false
	}

	userSession, err := h.authorizer.ValidateSession(session, cast.ByteArrayToString(userAgent), ip, cast.ByteArrayToString(location))
	if err != nil {
		writeError(ctx, err.Error(), fasthttp.StatusForbidden)
		return repo.UserSession{}, false
	}
	return userSession, true
}

func writeObject(ctx *fasthttp.RequestCtx, obj any, status int) {
	row, err := json.Marshal(obj)
	if err != nil {
		ctx.SetStatusCode(fasthttp.StatusInternalServerError)
		return
	}

	ctx.SetStatusCode(status)
	ctx.Response.Header.Set(fasthttp.HeaderContentType, "application/json")
	_, _ = ctx.Write(row)
}

type errorResponse struct {
	Error string `json:"error"`
}

func writeError(ctx *fasthttp.RequestCtx, message string, status int) {
	response := errorResponse{Error: message}
	row, err := json.Marshal(&response)
	if err != nil {
		ctx.SetStatusCode(fasthttp.StatusInternalServerError)
		return
	}

	ctx.SetStatusCode(status)
	ctx.Response.Header.Set(fasthttp.HeaderContentType, "application/json")
	_, _ = ctx.Write(row)
}

func addCorsHeaders(ctx *fasthttp.RequestCtx) {
	ctx.Response.Header.Set("Access-Control-Allow-Origin", "*")
	ctx.Response.Header.Set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, PATCH")
	ctx.Response.Header.Set("Access-Control-Allow-Headers", "*")
	ctx.Response.Header.Set("Access-Control-Allow-Credentials", "true")
}
